const choiceBox1 = document.querySelector('#choice1');
const choiceBox2 = document.querySelector('#choice2');

const inputsList1 = document.querySelector('#inputlist1');
const inputsList2 = document.querySelector('#inputlist2');

const queryChoiceInputs1 = () => document.querySelectorAll('#inputlist1 input');
const queryChoiceInputs2 = () => document.querySelectorAll('#inputlist2 input');

const settingsButton = document.querySelector('#settingsbutton');

const saveButton = document.querySelector('#savebutton');
const cancelButton = document.querySelector('#cancelbutton');

const addButton1 = document.querySelector('#addbutton1');
const addButton2 = document.querySelector('#addbutton2');
const removeButton1 = document.querySelector('#removebutton1');
const removeButton2 = document.querySelector('#removebutton2');

const rollButton = document.querySelector('#rollbutton');

const localStorageKey = 'list-roulette1'
const rollingCssClass = 'rolling';

rollButton.onclick = () => roll();
saveButton.onclick = () => save();
cancelButton.onclick = () => cancel();
settingsButton.onclick = () => toggleSettings();
addButton1.onclick = () => addItem(inputsList1);
addButton2.onclick = () => addItem(inputsList2);
removeButton1.onclick = () => removeItem(inputsList1);
removeButton2.onclick = () => removeItem(inputsList2);

// Rolling

let rollingState = {};

function roll() {
    if (hasFinishedRolling()) {
        rollButton.classList.add('disabled');
    
        rollingState = [true, true];

        startRolling(0, choiceBox1, queryChoiceInputs1());
        startRolling(1, choiceBox2, queryChoiceInputs2());
    }
}

function startRolling(stateIndex, choiceBox, choiceInputs) {
    choiceBox.classList.add(rollingCssClass);

    const choiceList = [...choiceInputs]
        .map(element => element.value)
        .filter(value => value);

    const stepCount = randomIntBetween(20, 30);
    const initialDuration = randomBetween(0.1, 0.4);

    rollOneStep(stateIndex, stepCount, initialDuration, choiceList, choiceBox);
}

function rollOneStep(stateIndex, remainingStepCount, duration, choiceList, choiceBox) {
    const choice = choiceList[randomIntBetween(0, choiceList.length)];
    choiceBox.innerHTML = choice;

    remainingStepCount -= 1;
    duration *= 1.15;
    duration = Math.min(duration, 1000);

    if (remainingStepCount > 0) {
        setTimeout(() => {
            rollOneStep(stateIndex, remainingStepCount, duration, choiceList, choiceBox);
        }, duration)
    } else {
        choiceBox.classList.remove(rollingCssClass);
        rollingState[stateIndex] = false;

        if (hasFinishedRolling()) {
            rollButton.classList.remove('disabled');
        }
    }
}

function hasFinishedRolling() {
    return ! Object.values(rollingState).some(rolling => rolling);
}

function randomBetween(min, max) {
    return min + Math.random() * (max - min);
}

function randomIntBetween(min, max) {
    return parseInt(randomBetween(min, max));
}

// Add and remove items

function addItem(list) {
    addInput(list);
    enableSaveAndCancelButtons();
}

function removeItem(list) {
    const inputs = list.querySelectorAll('input');

    if (inputs.length > 0) {
        inputs[inputs.length - 1].remove();
    }

    enableSaveAndCancelButtons();
}

function removeAllItems(list) {
    list.querySelectorAll('input')
        .forEach(input => input.remove());
}

function addInput(list) {
    const button = list.querySelector('div');
    const newInput = document.createElement('input');
    list.insertBefore(newInput, button);

    newInput.classList.add('form-control', 'mb-1');
    newInput.oninput = () => enableSaveAndCancelButtons();
    newInput.onchange = () => enableSaveAndCancelButtons();

    return newInput;
}

// Save & restore

function toggleSettings() {
    document.body.classList.toggle('configure');
}

function closeSettings() {
    document.body.classList.remove('configure');
}

function save() {
    const state = {
        list1: [],
        list2: [],
    };

    queryChoiceInputs1().forEach((input, index) => state.list1[index] = input.value);
    queryChoiceInputs2().forEach((input, index) => state.list2[index] = input.value);

    localStorage.setItem(localStorageKey, JSON.stringify(state));

    disableSaveAndCancelButtons();
}

function cancel() {
    restore();
}

function restore() {
    const stateStr = localStorage.getItem(localStorageKey);

    if (stateStr != null) {
        const state = JSON.parse(stateStr);

        removeAllItems(inputsList1);

        state.list1.forEach((value) => {
            const input = addInput(inputsList1);
            input.value = value;
        });

        removeAllItems(inputsList2);

        state.list2.forEach((value) => {
            const input = addInput(inputsList2);
            input.value = value;
        });
    }

    disableSaveAndCancelButtons();
}

function disableSaveAndCancelButtons() {
    saveButton.classList.add('disabled');
    cancelButton.classList.add('disabled');
}

function enableSaveAndCancelButtons() {
    saveButton.classList.remove('disabled');
    cancelButton.classList.remove('disabled');
}

restore();

// Shake detection

const myShakeEvent = new Shake({
    threshold: 15, // optional shake strength threshold
    timeout: 1000 // optional, determines the frequency of event generation
});

myShakeEvent.start();

window.addEventListener('shake', () => roll(), false);